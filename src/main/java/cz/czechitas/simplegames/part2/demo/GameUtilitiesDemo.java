package cz.czechitas.simplegames.part2.demo;

import java.awt.Image;

import javax.swing.ImageIcon;

public class GameUtilitiesDemo {

	public static final int STEP = 10;
	public static final int INIT_DOTS = 3;

	public static int getRandomInt(int maxValue) {
		return (int)(Math.random() * maxValue / STEP) * STEP;
	}
	
	public static Image loadImage(String imageName) {
		return new ImageIcon(imageName).getImage();
	}
}
