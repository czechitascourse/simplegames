package cz.czechitas.simplegames.part2.demo;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import cz.czechitas.simplegames.game.wrapper.CustomGameWindow;
import cz.czechitas.simplegames.game.wrapper.IGameProcessor;


public class SnakeGameBoardDemo implements IGameProcessor {

	CustomGameWindow window = new CustomGameWindow("Snake Game");
	JPanel gameBoard = window.getBoard();
	
	private ArrayList<PointDemo> listOfPoints = new ArrayList<>();

	private Image headImg;
	private Image bodyImg;

	private AppleDemo apple;

	public SnakeGameBoardDemo() {

		// Need to add the game processor to the game window
		window.addProcessor(this);

		headImg = loadImage("head.png");
		bodyImg = loadImage("body.png");

		for (int i = 0; i < GameUtilitiesDemo.INIT_DOTS; i++) {
			PointDemo newPoint = new PointDemo(50 - i * GameUtilitiesDemo.STEP, 50);
			listOfPoints.add(newPoint);
		}

		apple = new AppleDemo(window.getBoardWidth(), window.getBoardHeight());
	}

	private Image loadImage(String imageName) {
		ImageIcon ic = new ImageIcon(imageName);
		return ic.getImage();
	}

	public void doDrawing(Graphics g) {

		for (int i = 0; i < listOfPoints.size(); i++) {
			PointDemo p = listOfPoints.get(i);
			if (i == 0) {
				g.drawImage(headImg, p.getX(), p.getY(), gameBoard);
			} else {
				g.drawImage(bodyImg, p.getX(), p.getY(), gameBoard);
			}
		}

		// Call draw apple
		apple.drawApple(g, gameBoard);
	}

	public void upKeyPressed() {
		// Get head point
		PointDemo headPoint = listOfPoints.get(0);
		
		int newHeadYCoord = headPoint.getY() - GameUtilitiesDemo.STEP;
		PointDemo newHeadPoint = new PointDemo(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	public void leftKeyPressed() {

		PointDemo headPoint = listOfPoints.get(0);
		int newHeadXCoord = headPoint.getX() - GameUtilitiesDemo.STEP;
		PointDemo newHeadPoint = new PointDemo(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	public void downKeyPressed() {

		PointDemo headPoint = listOfPoints.get(0);
		int newHeadYCoord = headPoint.getY() + GameUtilitiesDemo.STEP;
		PointDemo newHeadPoint = new PointDemo(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	public void rightKeyPressed() {

		PointDemo headPoint = listOfPoints.get(0);
		int newHeadXCoord = headPoint.getX() + GameUtilitiesDemo.STEP;
		PointDemo newHeadPoint = new PointDemo(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);
		
		window.repaintBoard();
	}
	
	private void moveSnake(PointDemo newHeadPoint) {
		// Check if the next move will cross the screen border
		checkBoardCollisions(newHeadPoint);

		// Check if the apple should be eaten
		checkApple(newHeadPoint);
		
		// Move snake body
		moveSnakeBody(newHeadPoint);
		
	}

	private void moveSnakeBody(PointDemo newHeadPoint) {

		// Start with the last segment and loop through all but the head
		for (int i = listOfPoints.size() - 1; i > 0; i--) {

			PointDemo currentPoint = listOfPoints.get(i); // segment No. i
			PointDemo previousPoint = listOfPoints.get(i - 1); // segment No. i-1

			// Set the current segment X coordinate to the previous segment's X
			int prevX = previousPoint.getX();
			currentPoint.setX(prevX);

			// Set the current segment Y coordinate to the previous segment's Y
			int prevY = previousPoint.getY();
			currentPoint.setY(prevY);

			// Short version:
			// listOfPoints.get(i).setX(listOfPoints.get(i-1).getX());
			// listOfPoints.get(i).setY(listOfPoints.get(i-1).getY());
		}
		
		listOfPoints.set(0, newHeadPoint);
	}
	
	private void checkApple(PointDemo snakeHeadPoint) {
		// Check the apple
		if (snakeHeadPoint.getX() == apple.getX() 
				&& snakeHeadPoint.getY() == apple.getY()) {
			listOfPoints.add(new PointDemo());

			apple.setRandomLocation();
		}
	}
	
	private void checkBoardCollisions(PointDemo snakeHeadPoint) {
		if (snakeHeadPoint.getY() < 0) {
			snakeHeadPoint.setY(window.getBoardHeight() - GameUtilitiesDemo.STEP);
		}
		
		if (snakeHeadPoint.getX() < 0) {
			snakeHeadPoint.setX(window.getBoardWidth() - GameUtilitiesDemo.STEP);
		}
		
		if (snakeHeadPoint.getY() > window.getBoardHeight() - GameUtilitiesDemo.STEP) {
			snakeHeadPoint.setY(0);
		}
		
		if (snakeHeadPoint.getX() > window.getBoardWidth() - GameUtilitiesDemo.STEP) {
			snakeHeadPoint.setX(0);
		}
	}

	public JFrame start() {
		return window.getWindow();
	}

}
