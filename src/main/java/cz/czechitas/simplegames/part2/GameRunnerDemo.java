package cz.czechitas.simplegames.part2;

import cz.czechitas.simplegames.game.wrapper.BoardRunner;
import cz.czechitas.simplegames.part2.demo.SnakeGameBoardDemo;

public class GameRunnerDemo {

	public static void main(String[] args) {
		BoardRunner.runGame(new SnakeGameBoardDemo());
	}
}