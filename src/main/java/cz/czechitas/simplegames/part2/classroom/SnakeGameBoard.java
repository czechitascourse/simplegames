package cz.czechitas.simplegames.part2.classroom;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import cz.czechitas.simplegames.game.wrapper.CustomGameWindow;
import cz.czechitas.simplegames.game.wrapper.IGameProcessor;


public class SnakeGameBoard implements IGameProcessor {

	CustomGameWindow window = new CustomGameWindow("Move image with keys");
	JPanel gameBoard = window.getBoard();

	private Image myImg;
	private int x = 50;
	private int y = 50;

	private static final int STEP = 10;

	public SnakeGameBoard() {

		// Need to add the game processor to the game window
		window.addProcessor(this);

		loadImage("head.png");
	}

	private void loadImage(String imageName) {
		ImageIcon ic = new ImageIcon(imageName);
		myImg = ic.getImage();

	}

	public void leftKeyPressed() {
		if (x - STEP >= 0) {
			x -= STEP;
			window.repaintBoard();
		}
	}

	public void rightKeyPressed() {
		if (x + STEP <= gameBoard.getWidth() - myImg.getWidth(null)) {
			x += STEP;
			window.repaintBoard();
		}
	}

	public void downKeyPressed() {
		if (y + STEP <= gameBoard.getHeight() - myImg.getHeight(null)) {
			y += STEP;
			window.repaintBoard();
		}
	}

	public void upKeyPressed() {
		if (y - STEP >= 0) {
			y -= STEP;
			window.repaintBoard();
		}
	}

	@Override
	public JFrame start() {
		return window.getWindow();
	}

	@Override
	public void doDrawing(Graphics g) {
		g.drawImage(myImg, x, y, gameBoard);
		
	}
}
