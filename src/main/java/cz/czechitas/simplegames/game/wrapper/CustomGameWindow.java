package cz.czechitas.simplegames.game.wrapper;

import java.awt.Color;

import javax.swing.JFrame;

public class CustomGameWindow extends ProgramWindow {

	public CustomGameWindow(String title) {
		super(title);

		setBoardBackground(Color.black);
		setBoardDimensions(500, 300);
		
		makeWindowResizable(false);
	}
	
	public void addProcessor(IGameProcessor processor) {
		addBoardKeyListener(new SimpleArrowKeyListener(processor));
		setGameProcessor(processor);
	}
	
	public JFrame startGame() {
		return getWindow();
	}

}
