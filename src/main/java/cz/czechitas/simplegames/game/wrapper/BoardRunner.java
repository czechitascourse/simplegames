package cz.czechitas.simplegames.game.wrapper;

import java.awt.EventQueue;

import javax.swing.JFrame;

public class BoardRunner {
	
	public static void runGame(IGameProcessor gameBoard) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {                
                JFrame ex = gameBoard.start();
                ex.setVisible(true);
            }
        });
	}
}
