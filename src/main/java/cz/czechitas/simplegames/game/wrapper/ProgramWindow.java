package cz.czechitas.simplegames.game.wrapper;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class ProgramWindow extends JFrame {
	private static final long serialVersionUID = -6696410798362903167L;
	
	private ProgramPanel board;
	IGameProcessor gameProcessor;

	private ProgramWindow() {
		this.board = new ProgramPanel();
		add(board);
		pack();
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public ProgramWindow(String title) {
		this();
		setTitle(title);
	}
	
	public void addBoardKeyListener(KeyAdapter listener) {
		board.addKeyListener(listener);
		board.setFocusable(true);
	}
	
	public void repaintBoard() {
		board.repaint();
	}
	
	public void setBoardBackground(Color bg) {
		board.setBackground(bg);
	}
	
	public int getBoardWidth() {
		return board.getWidth();
	}
	
	public int getBoardHeight() {
		return board.getHeight();
	}
	
	public JPanel getBoard() {
		return board;
	}
	
	public JFrame getWindow() {
		return this;
	}
	
	public void setBoardDimensions(int x, int y) {
		board.setPreferredSize(new Dimension(x, y));
		pack();
	}
	
	public void makeWindowResizable(boolean val) {
		setResizable(val);
		pack();
	}
	
	public void setGameProcessor(IGameProcessor processor) {
		this.gameProcessor = processor;
	}

	
	
	/* Wrapper around JPanel methods. */
	private class ProgramPanel extends JPanel {

		private static final long serialVersionUID = 2792392005683912513L;
	
	    @Override
	    public void paintComponent(Graphics g) {
	        super.paintComponent(g);
	        
	        gameProcessor.doDrawing(g);
	        
	        Toolkit.getDefaultToolkit().sync();
	    }
		
	}
}
