package cz.czechitas.simplegames.game.wrapper;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class SimpleArrowKeyListener extends KeyAdapter {
	IGameProcessor processor;
	
	public SimpleArrowKeyListener(IGameProcessor processor) {
		this.processor = processor;
	}
	
	public void setBoard(IGameProcessor processor) {
		this.processor = processor;
	}
	
    @Override
    public void keyPressed(KeyEvent e) {

        int key = e.getKeyCode();

        if (key == KeyEvent.VK_LEFT) {
        	processor.leftKeyPressed();
        }

        if (key == KeyEvent.VK_RIGHT) {
        	processor.rightKeyPressed();
        }

        if (key == KeyEvent.VK_UP) {
        	processor.upKeyPressed();
        }

        if (key == KeyEvent.VK_DOWN) {
        	processor.downKeyPressed();
        }
    }
}
