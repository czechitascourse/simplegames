package cz.czechitas.simplegames.game.wrapper;

import java.awt.Graphics;
import javax.swing.JFrame;

public interface IGameProcessor {
	
	public void leftKeyPressed();
	public void downKeyPressed();
	public void upKeyPressed();
	public void rightKeyPressed();
	public void doDrawing(Graphics g);
	public JFrame start();

}
