package cz.czechitas.simplegames.part3.empty;

public class PointEmpty {
	private int x;
	private int y;
		
	public PointEmpty() {
		this(0, 0);
	}
	
	public PointEmpty(int x, int y) {
		this.x = x;
		this.y = y;
	}

	protected int getX() {
		return x;
	}

	protected void setX(int x) {
		this.x = x;
	}

	protected int getY() {
		return y;
	}

	protected void setY(int y) {
		this.y = y;
	}
}
