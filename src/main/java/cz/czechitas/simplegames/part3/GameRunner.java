package cz.czechitas.simplegames.part3;

import cz.czechitas.simplegames.game.wrapper.BoardRunner;
import cz.czechitas.simplegames.part3.classroom.SnakeGameBoard;

public class GameRunner {

	public static void main(String[] args) {
		BoardRunner.runGame(new SnakeGameBoard());
	}
}