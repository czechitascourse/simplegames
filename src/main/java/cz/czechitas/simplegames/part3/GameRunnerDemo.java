package cz.czechitas.simplegames.part3;

import cz.czechitas.simplegames.game.wrapper.BoardRunner;
import cz.czechitas.simplegames.part3.demo.SnakeGameBoardDemo;

public class GameRunnerDemo {

	public static void main(String[] args) {
		BoardRunner.runGame(new SnakeGameBoardDemo());
	}
}