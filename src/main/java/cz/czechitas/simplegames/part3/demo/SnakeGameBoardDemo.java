package cz.czechitas.simplegames.part3.demo;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import cz.czechitas.simplegames.game.wrapper.CustomGameWindow;
import cz.czechitas.simplegames.game.wrapper.IGameProcessor;

public class SnakeGameBoardDemo implements IGameProcessor {

	CustomGameWindow window = new CustomGameWindow("Snake Game");
	JPanel gameBoard = window.getBoard();

	private ArrayList<PointDemo> listOfPoints = new ArrayList<>();

	private Image headImg;
	private Image bodyImg;

	private AppleDemo apple;

	private boolean inGame;

	public SnakeGameBoardDemo() {

		// Need to add the game processor to the game window
		window.addProcessor(this);

		headImg = loadImage("head.png");
		bodyImg = loadImage("body.png");

		for (int i = 0; i < GameUtilitiesDemo.INIT_DOTS; i++) {
			PointDemo newPoint = new PointDemo(50 - i * GameUtilitiesDemo.STEP, 50);
			listOfPoints.add(newPoint);
		}

		apple = new AppleDemo(window.getBoardWidth(), window.getBoardHeight());

		inGame = true;
	}

	private Image loadImage(String imageName) {
		ImageIcon ic = new ImageIcon(imageName);
		return ic.getImage();
	}

	@Override
	public void doDrawing(Graphics g) {
		drawGame(g);
		if (!inGame) {
			GameUtilitiesDemo.drawGameOver(g, gameBoard);
		}
	}

	private void drawGame(Graphics g) {

		for (int i = listOfPoints.size() - 1; i > 0; i--) {
			PointDemo p = listOfPoints.get(i);

			g.drawImage(bodyImg, p.getX(), p.getY(), gameBoard);
		}
		g.drawImage(headImg, listOfPoints.get(0).getX(), listOfPoints.get(0).getY(), gameBoard);

		// Call draw apple
		apple.drawApple(g, gameBoard);
	}

	@Override
	public void upKeyPressed() {
		// Get head point
		PointDemo headPoint = listOfPoints.get(0);
		
		int newHeadYCoord = headPoint.getY() - GameUtilitiesDemo.STEP;
		PointDemo newHeadPoint = new PointDemo(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	@Override
	public void leftKeyPressed() {

		PointDemo headPoint = listOfPoints.get(0);
		int newHeadXCoord = headPoint.getX() - GameUtilitiesDemo.STEP;
		PointDemo newHeadPoint = new PointDemo(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	@Override
	public void downKeyPressed() {

		PointDemo headPoint = listOfPoints.get(0);
		int newHeadYCoord = headPoint.getY() + GameUtilitiesDemo.STEP;
		PointDemo newHeadPoint = new PointDemo(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	@Override
	public void rightKeyPressed() {

		PointDemo headPoint = listOfPoints.get(0);
		int newHeadXCoord = headPoint.getX() + GameUtilitiesDemo.STEP;
		PointDemo newHeadPoint = new PointDemo(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);
		
		window.repaintBoard();
	}

	private void moveSnake(PointDemo newHeadPoint) {
		// Check if the next move will cross the screen border
		checkBoardCollisions(newHeadPoint);

		// Check if the apple should be eaten
		checkApple(newHeadPoint);
		
		// Move snake body
		moveSnakeBody(newHeadPoint);
		
	}

	private void moveSnakeBody(PointDemo newHeadPoint) {

		// Start with the last segment and loop through all but the head
		for (int i = listOfPoints.size() - 1; i > 0; i--) {

			PointDemo currentPoint = listOfPoints.get(i); // segment No. i
			PointDemo previousPoint = listOfPoints.get(i - 1); // segment No. i-1

			// Set the current segment X coordinate to the previous segment's X
			int prevX = previousPoint.getX();
			currentPoint.setX(prevX);

			// Set the current segment Y coordinate to the previous segment's Y
			int prevY = previousPoint.getY();
			currentPoint.setY(prevY);

			// Short version:
			// listOfPoints.get(i).setX(listOfPoints.get(i-1).getX());
			// listOfPoints.get(i).setY(listOfPoints.get(i-1).getY());
		}
		
		listOfPoints.set(0, newHeadPoint);
	}
	
	private void checkApple(PointDemo snakeHeadPoint) {
		// Check the apple
		if (snakeHeadPoint.getX() == apple.getX() 
				&& snakeHeadPoint.getY() == apple.getY()) {
			listOfPoints.add(new PointDemo());

			apple.setRandomLocation();
		}
	}
	
	private void checkBoardCollisions(PointDemo snakeHeadPoint) {
		// Check the borders
		int x = snakeHeadPoint.getX();
		int y = snakeHeadPoint.getY();
		
		if (x < 0 || x >= gameBoard.getWidth() || y < 0 || y >= gameBoard.getHeight()) {
			inGame = false;
		}
		
		// Check the body
		for (PointDemo point : listOfPoints) {
			if (point.getX() == x && point.getY() == y) {
				inGame = false;
			}
		} 
	}

	@Override
	public JFrame start() {
		return window.getWindow();
	}

}
