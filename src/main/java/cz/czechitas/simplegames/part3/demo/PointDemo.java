package cz.czechitas.simplegames.part3.demo;

public class PointDemo {
	private int x;
	private int y;
		
	public PointDemo() {
		this(0, 0);
	}
	
	public PointDemo(int x, int y) {
		this.x = x;
		this.y = y;
	}

	protected int getX() {
		return x;
	}

	protected void setX(int x) {
		this.x = x;
	}

	protected int getY() {
		return y;
	}

	protected void setY(int y) {
		this.y = y;
	}
}
