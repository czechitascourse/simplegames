package cz.czechitas.simplegames.part3.classroom;

import java.awt.Image;

import javax.swing.ImageIcon;

public class GameUtilities {

	public static final int STEP = 10;
	public static final int INIT_DOTS = 3;

	public static int getRandomInt(int maxValue) {
		return (int)(Math.random() * maxValue / STEP) * STEP;
	}
	
	public static Image loadImage(String imageName) {
		return new ImageIcon(imageName).getImage();
	}
}
