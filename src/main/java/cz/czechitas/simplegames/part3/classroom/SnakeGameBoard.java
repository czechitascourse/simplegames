package cz.czechitas.simplegames.part3.classroom;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import cz.czechitas.simplegames.game.wrapper.CustomGameWindow;
import cz.czechitas.simplegames.game.wrapper.IGameProcessor;

/**
 * This Demo is a copy of SnakeGameBoardDemo 
 * with one amendment in doDrawing method
 */
public class SnakeGameBoard implements IGameProcessor {

	private int snakeDots;

	CustomGameWindow window = new CustomGameWindow("Snake Game");
	JPanel gameBoard = window.getBoard();
	
	private ArrayList<Point> listOfPoints = new ArrayList<>();

	private Image headImg;
	private Image bodyImg;

	private Apple apple;

	public SnakeGameBoard() {

		// Need to add the game processor to the game window
		window.addProcessor(this);

		headImg = loadImage("head.png");
		bodyImg = loadImage("body.png");

		snakeDots = GameUtilities.INIT_DOTS;
		for (int i = 0; i < snakeDots; i++) {
			Point newPoint = new Point(50 - i * GameUtilities.STEP, 50);
			listOfPoints.add(newPoint);
		}

		apple = new Apple(window.getBoardWidth(), window.getBoardHeight());
	}

	private Image loadImage(String imageName) {
		ImageIcon ic = new ImageIcon(imageName);
		return ic.getImage();
	}

	/**
	 * Changed the order of displaying the Snake's body and head
	 * segments on canvas. This way, the head appears on the top
	 * of the body segments since it was added last.
	 */
	public void doDrawing(Graphics g) {

		for (int i = listOfPoints.size() - 1; i > 0; i--) {
			Point p = listOfPoints.get(i);

			g.drawImage(bodyImg, p.getX(), p.getY(), gameBoard);
		}
		g.drawImage(headImg, listOfPoints.get(0).getX(), listOfPoints.get(0).getY(), gameBoard);

		// Call draw apple
		apple.drawApple(g, gameBoard);
	}

	public void upKeyPressed() {
		// Get head point
		Point headPoint = listOfPoints.get(0);
		
		int newHeadYCoord = headPoint.getY() - GameUtilities.STEP;
		Point newHeadPoint = new Point(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	public void leftKeyPressed() {

		Point headPoint = listOfPoints.get(0);
		int newHeadXCoord = headPoint.getX() - GameUtilities.STEP;
		Point newHeadPoint = new Point(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	public void downKeyPressed() {

		Point headPoint = listOfPoints.get(0);
		int newHeadYCoord = headPoint.getY() + GameUtilities.STEP;
		Point newHeadPoint = new Point(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	public void rightKeyPressed() {

		Point headPoint = listOfPoints.get(0);
		int newHeadXCoord = headPoint.getX() + GameUtilities.STEP;
		Point newHeadPoint = new Point(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);
		
		window.repaintBoard();
	}
	
	private void moveSnake(Point newHeadPoint) {
		// Check if the next move will cross the screen border
		checkBoardCollisions(newHeadPoint);

		// Check if the apple should be eaten
		checkApple(newHeadPoint);
		
		// Move snake body
		moveSnakeBody(newHeadPoint);
		
	}

	private void moveSnakeBody(Point newHeadPoint) {

		// Start with the last segment and loop through all but the head
		for (int i = listOfPoints.size() - 1; i > 0; i--) {

			Point currentPoint = listOfPoints.get(i); // segment No. i
			Point previousPoint = listOfPoints.get(i - 1); // segment No. i-1

			// Set the current segment X coordinate to the previous segment's X
			int prevX = previousPoint.getX();
			currentPoint.setX(prevX);

			// Set the current segment Y coordinate to the previous segment's Y
			int prevY = previousPoint.getY();
			currentPoint.setY(prevY);

			// Short version:
			// listOfPoints.get(i).setX(listOfPoints.get(i-1).getX());
			// listOfPoints.get(i).setY(listOfPoints.get(i-1).getY());
		}
		
		listOfPoints.set(0, newHeadPoint);
	}
	
	private void checkApple(Point snakeHeadPoint) {
		// Check the apple
		if (snakeHeadPoint.getX() == apple.getX() 
				&& snakeHeadPoint.getY() == apple.getY()) {
			listOfPoints.add(new Point());

			apple.setRandomLocation();
		}
	}
	
	private void checkBoardCollisions(Point snakeHeadPoint) {
		if (snakeHeadPoint.getY() < 0) {
			snakeHeadPoint.setY(window.getBoardHeight() - GameUtilities.STEP);
		}
		
		if (snakeHeadPoint.getX() < 0) {
			snakeHeadPoint.setX(window.getBoardWidth() - GameUtilities.STEP);
		}
		
		if (snakeHeadPoint.getY() > window.getBoardHeight() - GameUtilities.STEP) {
			snakeHeadPoint.setY(0);
		}
		
		if (snakeHeadPoint.getX() > window.getBoardWidth() - GameUtilities.STEP) {
			snakeHeadPoint.setX(0);
		}
	}

	public JFrame start() {
		return window.getWindow();
	}

}
