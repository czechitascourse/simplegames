package cz.czechitas.simplegames.part1;

import cz.czechitas.simplegames.game.wrapper.BoardRunner;
import cz.czechitas.simplegames.part1.classroom.SnakeGameBoard;

public class GameRunner {

	public static void main(String[] args) {
		
		// Create new object of type SnakeGameBoard
		SnakeGameBoard board = new SnakeGameBoard();
		
		// Pass the new object as a parameter to the BoardRunner class
		BoardRunner.runGame(board);
	}
}