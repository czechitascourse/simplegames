package cz.czechitas.simplegames.part1.classroom;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import cz.czechitas.simplegames.game.wrapper.CustomGameWindow;
import cz.czechitas.simplegames.game.wrapper.IGameProcessor;

public class SnakeGameBoard implements IGameProcessor {

	// This is a customized game window that has most of the properties set
	CustomGameWindow window = new CustomGameWindow("Move image with keys");
	JPanel gameBoard = window.getBoard();

	// These fields hold the sprite coordinates
	private int x;
	private int y;
	
	// This field holds the image name
	private String imageName; 

	// This field holds an Image
	private Image myImg;

	public SnakeGameBoard() {
		// Adding this game processor to the game window
		window.addProcessor(this);

		// Here we initialize the sprite
		ImageIcon ic = new ImageIcon(imageName);
		myImg = ic.getImage();
		
	}

	
	public void upKeyPressed() {
		
		// Refresh the window
		window.repaintBoard();
		
	}
	
	public void leftKeyPressed() {
		
		// Refresh the window
		window.repaintBoard();
		
	}
	
	public void downKeyPressed() {
		
		// Refresh the window
		window.repaintBoard();
		
	}

	public void rightKeyPressed() {
		
		// Refresh the window
		window.repaintBoard();
		
	}

	@Override
	public JFrame start() {
		return window.getWindow();
	}

	@Override
	public void doDrawing(Graphics g) {
		g.drawImage(myImg, x, y, gameBoard);
		
	}
}
