package cz.czechitas.simplegames.part4;

import cz.czechitas.simplegames.game.wrapper.BoardRunner;
import cz.czechitas.simplegames.part4.classroom.SnakeGameBoard;

public class TemporaryGameRunner {

	public static void main(String[] args) {
		BoardRunner.runGame(new SnakeGameBoard());
	}
}