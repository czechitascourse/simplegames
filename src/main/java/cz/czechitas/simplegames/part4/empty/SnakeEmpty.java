package cz.czechitas.simplegames.part4.empty;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

import javax.swing.JPanel;

public class SnakeEmpty {
	private Image headImg;
	private Image bodyImg;
	
	private ArrayList<PointEmpty> listOfPoints = new ArrayList<>();
	
	public SnakeEmpty() {
		headImg = GameUtilitiesEmpty.loadImage("head.png");
		bodyImg = GameUtilitiesEmpty.loadImage("body.png");

		for (int i = 0; i < GameUtilitiesEmpty.INIT_DOTS; i++) {
			PointEmpty newPoint = new PointEmpty(50 - i * GameUtilitiesEmpty.STEP, 50);
			listOfPoints.add(newPoint);
		}
	}
	
	public void drawSnake(Graphics g, ImageObserver observer) {
		for (int i = listOfPoints.size() - 1; i > 0; i--) {
			PointEmpty p = listOfPoints.get(i);

			g.drawImage(bodyImg, p.getX(), p.getY(), observer);
		}
		g.drawImage(headImg, listOfPoints.get(0).getX(), listOfPoints.get(0).getY(), observer);
	}
	
	public void moveSnakeBody(PointEmpty newHeadPoint) {

		// Start with the last segment and loop through all but the head
		for (int i = listOfPoints.size() - 1; i > 0; i--) {

			PointEmpty currentPoint = listOfPoints.get(i); // segment No. i
			PointEmpty previousPoint = listOfPoints.get(i - 1); // segment No. i-1

			// Set the current segment X coordinate to the previous segment's X
			int prevX = previousPoint.getX();
			currentPoint.setX(prevX);

			// Set the current segment Y coordinate to the previous segment's Y
			int prevY = previousPoint.getY();
			currentPoint.setY(prevY);

			// Short version:
			// listOfPoints.get(i).setX(listOfPoints.get(i-1).getX());
			// listOfPoints.get(i).setY(listOfPoints.get(i-1).getY());
		}
		
		listOfPoints.set(0, newHeadPoint);
	}
	
	public void checkApple(PointEmpty snakeHeadPoint, AppleEmpty apple) {
		// Check the apple
		if (snakeHeadPoint.getX() == apple.getX() 
				&& snakeHeadPoint.getY() == apple.getY()) {
			listOfPoints.add(new PointEmpty());

			apple.setRandomLocation();
		}
	}
	
	public boolean checkBoardCollisions(PointEmpty snakeHeadPoint, JPanel gameBoard) {
		// Check the borders
		int x = snakeHeadPoint.getX();
		int y = snakeHeadPoint.getY();
		
		if (x < 0 || x >= gameBoard.getWidth() || y < 0 || y >= gameBoard.getHeight()) {
			return false;
		}
		
		// Check the body
		for (PointEmpty point : listOfPoints) {
			if (point.getX() == x && point.getY() == y) {
				return false;
			}
		} 
		return true;
	}
	
	public PointEmpty getHeadPoint() {
		return listOfPoints.get(0);
	}
	
}
