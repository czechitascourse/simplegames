package cz.czechitas.simplegames.part4.empty;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import cz.czechitas.simplegames.game.wrapper.CustomGameWindow;
import cz.czechitas.simplegames.game.wrapper.IGameProcessor;

public class SnakeGameBoardEmpty implements IGameProcessor {

	CustomGameWindow window = new CustomGameWindow("Snake Game");
	JPanel gameBoard = window.getBoard();

	private AppleEmpty apple;
	private SnakeEmpty snake;

	private boolean inGame;

	public SnakeGameBoardEmpty() {

		// Need to add the game processor to the game window
		window.addProcessor(this);

		apple = new AppleEmpty(window.getBoardWidth(), window.getBoardHeight());
		snake = new SnakeEmpty();

		inGame = true;
	}

	@Override
	public void doDrawing(Graphics g) {
		
		drawGame(g);
		if (!inGame) {
			GameUtilitiesEmpty.drawGameOver(g, gameBoard);
		}
	}

	private void drawGame(Graphics g) {

		// Call draw snake
		snake.drawSnake(g, gameBoard);
		// Call draw apple
		apple.drawApple(g, gameBoard);
	}

	@Override
	public void upKeyPressed() {
		// Get head point
		PointEmpty headPoint = snake.getHeadPoint();
		
		int newHeadYCoord = headPoint.getY() - GameUtilitiesEmpty.STEP;
		PointEmpty newHeadPoint = new PointEmpty(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	@Override
	public void leftKeyPressed() {

		PointEmpty headPoint = snake.getHeadPoint();
		int newHeadXCoord = headPoint.getX() - GameUtilitiesEmpty.STEP;
		PointEmpty newHeadPoint = new PointEmpty(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	@Override
	public void downKeyPressed() {

		PointEmpty headPoint = snake.getHeadPoint();
		int newHeadYCoord = headPoint.getY() + GameUtilitiesEmpty.STEP;
		PointEmpty newHeadPoint = new PointEmpty(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	@Override
	public void rightKeyPressed() {

		PointEmpty headPoint = snake.getHeadPoint();
		int newHeadXCoord = headPoint.getX() + GameUtilitiesEmpty.STEP;
		PointEmpty newHeadPoint = new PointEmpty(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);
		
		window.repaintBoard();
	}

	private void moveSnake(PointEmpty newHeadPoint) {
		// Check if the next move will cross the screen border
		inGame = snake.checkBoardCollisions(newHeadPoint, gameBoard);

		// Check if the apple should be eaten
		snake.checkApple(newHeadPoint, apple);
		
		// Move snake body
		snake.moveSnakeBody(newHeadPoint);
		
	}

	@Override
	public JFrame start() {
		return window.getWindow();
	}

}
