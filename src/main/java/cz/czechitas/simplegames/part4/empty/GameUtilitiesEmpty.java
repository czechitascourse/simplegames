package cz.czechitas.simplegames.part4.empty;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class GameUtilitiesEmpty {

	public static final int STEP = 10;
	public static final int INIT_DOTS = 3;
	public static final String GAME_OVER = "Game Over!";

	public static int getRandomInt(int maxValue) {
		return (int)(Math.random() * maxValue / STEP) * STEP;
	}
	
	public static Image loadImage(String imageName) {
		return new ImageIcon(imageName).getImage();
	}
	
	public static void drawGameOver(Graphics g, JPanel gameBoard) {
		// Define font (name, style, size)
		Font font = new Font("Verdana", Font.BOLD, 24);

		// Get font metrics (careful with the package!) to calculate the width of the string
		FontMetrics metr = gameBoard.getFontMetrics(font);
		int strWidth = metr.stringWidth(GAME_OVER);

		g.setColor(Color.WHITE);
		g.setFont(font);
		g.drawString(GAME_OVER,
				(gameBoard.getWidth() - strWidth) / 2,
				gameBoard.getHeight() / 2);
	}
}
