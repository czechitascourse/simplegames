package cz.czechitas.simplegames.part4.empty;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;

public class AppleEmpty {
	
	private PointEmpty appleLocation;
	private Image appleImg;
	
	private int boardWidth;
	private int boardHeight;
	
	public AppleEmpty(int boardWidth, int boardHeight) {
		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
		
		appleImg = GameUtilitiesEmpty.loadImage("apple.png");
		setRandomLocation();
	}
	
	public void setRandomLocation() {
		// Calculate random X
		int x = GameUtilitiesEmpty.getRandomInt(boardWidth);
		// Calculate random Y
		int y = GameUtilitiesEmpty.getRandomInt(boardHeight);
		
		// Set new coordinates to the Point
		appleLocation = new PointEmpty(x, y);
	}
	
	public void drawApple(Graphics g, ImageObserver observer) {
		g.drawImage(appleImg, appleLocation.getX(), appleLocation.getY(), observer);		
	}
	
	public int getX() {
		return appleLocation.getX();
	}
	
	public int getY() {
		return appleLocation.getY();
	}

}
