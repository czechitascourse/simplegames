package cz.czechitas.simplegames.part4.demo;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

import javax.swing.JPanel;

public class SnakeDemo {
	
	private Image headImg;
	private Image bodyImg;
	
	private String direction;
	
	private ArrayList<PointDemo> listOfPoints = new ArrayList<>();
	
	public SnakeDemo() {
		headImg = GameUtilitiesDemo.loadImage("head.png");
		bodyImg = GameUtilitiesDemo.loadImage("body.png");

		for (int i = 0; i < GameUtilitiesDemo.INIT_DOTS; i++) {
			PointDemo newPoint = new PointDemo(50 - i * GameUtilitiesDemo.STEP, 50);
			listOfPoints.add(newPoint);
		}
		
		direction = GameUtilitiesDemo.RIGHT;
	}
	
	public void drawSnake(Graphics g, ImageObserver observer) {
		for (int i = listOfPoints.size() - 1; i > 0; i--) {
			PointDemo p = listOfPoints.get(i);

			g.drawImage(bodyImg, p.getX(), p.getY(), observer);
		}
		g.drawImage(headImg, listOfPoints.get(0).getX(), listOfPoints.get(0).getY(), observer);
	}

	public void moveSnakeBody(PointDemo newHeadPoint) {

		// Start with the last segment and loop through all but the head
		for (int i = listOfPoints.size() - 1; i > 0; i--) {

			PointDemo currentPoint = listOfPoints.get(i); // segment No. i
			PointDemo previousPoint = listOfPoints.get(i - 1); // segment No. i-1

			// Set the current segment X coordinate to the previous segment's X
			int prevX = previousPoint.getX();
			currentPoint.setX(prevX);

			// Set the current segment Y coordinate to the previous segment's Y
			int prevY = previousPoint.getY();
			currentPoint.setY(prevY);

			// Short version:
			// listOfPoints.get(i).setX(listOfPoints.get(i-1).getX());
			// listOfPoints.get(i).setY(listOfPoints.get(i-1).getY());
		}
		
		listOfPoints.set(0, newHeadPoint);
	}
	
	public void checkApple(PointDemo snakeHeadPoint, AppleDemo apple) {
		// Check the apple
		if (snakeHeadPoint.getX() == apple.getX() 
				&& snakeHeadPoint.getY() == apple.getY()) {
			listOfPoints.add(new PointDemo());

			apple.setRandomLocation();
		}
	}
	
	public boolean checkBoardCollisions(PointDemo snakeHeadPoint, JPanel gameBoard) {
		// Check the borders
		int x = snakeHeadPoint.getX();
		int y = snakeHeadPoint.getY();
		
		if (x < 0 || x >= gameBoard.getWidth() || y < 0 || y >= gameBoard.getHeight()) {
			return false;
		}
		
		// Check the body
		for (PointDemo point : listOfPoints) {
			if (point.getX() == x && point.getY() == y) {
				return false;
			}
		} 
		return true;
	}
	
	public PointDemo calculateNextHeadPoint() {
		PointDemo headPoint = listOfPoints.get(0);
		
		int newHeadXCoord = headPoint.getX();
		int newHeadYCoord = headPoint.getY();
		
		if (direction.equals(GameUtilitiesDemo.UP)) {
			newHeadYCoord = headPoint.getY() - GameUtilitiesDemo.STEP;
		} else if (direction.equals(GameUtilitiesDemo.LEFT)) {
			newHeadXCoord = headPoint.getX() - GameUtilitiesDemo.STEP;
		} else if (direction.equals(GameUtilitiesDemo.DOWN)) {
			newHeadYCoord = headPoint.getY() + GameUtilitiesDemo.STEP;
		} else if (direction.equals(GameUtilitiesDemo.RIGHT)) {
			newHeadXCoord = headPoint.getX() + GameUtilitiesDemo.STEP;
		}
		
		return new PointDemo(newHeadXCoord, newHeadYCoord);
	}

	protected String getDirection() {
		return direction;
	}

	protected void setDirection(String direction) {
		this.direction = direction;
	}

}
