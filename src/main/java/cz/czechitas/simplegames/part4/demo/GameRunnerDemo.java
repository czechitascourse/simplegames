package cz.czechitas.simplegames.part4.demo;

import java.awt.EventQueue;

public class GameRunnerDemo {
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SnakeGameWindowDemo snakeGameWindow = new SnakeGameWindowDemo();
					snakeGameWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
