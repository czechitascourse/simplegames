package cz.czechitas.simplegames.part4.demo;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class SnakeGameBoardDemo extends JPanel implements ActionListener {

	private static final long serialVersionUID = 483086571065152481L;

	private String userName;
	private int userScore = 0;

	private boolean inGame;

	private AppleDemo apple;
	private SnakeDemo snake;

	private Timer timer;

	/**
	 * Create the panel.
	 */
	public SnakeGameBoardDemo() {
		setPreferredSize(new Dimension(500, 300));
		userName = "No Name";
		inGame = true;
		timer = new Timer(GameUtilitiesDemo.TIMER_DELAY, this);
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		doDrawing(g);
		Toolkit.getDefaultToolkit().sync();
	}

	public void doDrawing(Graphics g) {
		drawGame(g);
		if (!inGame) {
			GameUtilitiesDemo.drawGameOver(g, this);
		}
	}

	private void drawGame(Graphics g) {
		// Call draw snake
		snake.drawSnake(g, this);

		// Call draw apple
		apple.drawApple(g, this);
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserName() {
		return userName;
	}

	public void initGameObjects() {
		inGame = true;
		apple = new AppleDemo(getWidth(), getHeight());
		snake = new SnakeDemo();
		timer.start();
	}

	public void changeDirection(String newDirection) {
		if (newDirection.equals(GameUtilitiesDemo.UP) 
				&& !snake.getDirection().equals(GameUtilitiesDemo.DOWN)) {
			snake.setDirection(GameUtilitiesDemo.UP);
		} else if (newDirection.equals(GameUtilitiesDemo.LEFT)
				&& !snake.getDirection().equals(GameUtilitiesDemo.RIGHT)) {
			snake.setDirection(GameUtilitiesDemo.LEFT);
		} else if (newDirection.equals(GameUtilitiesDemo.DOWN) 
				&& !snake.getDirection().equals(GameUtilitiesDemo.UP)) {
			snake.setDirection(GameUtilitiesDemo.DOWN);
		} else if (newDirection.equals(GameUtilitiesDemo.RIGHT)
				&& !snake.getDirection().equals(GameUtilitiesDemo.LEFT)) {
			snake.setDirection(GameUtilitiesDemo.RIGHT);
		}
	}

	private void moveSnake(PointDemo newHeadPoint) {
		// Check if the next move will cross the screen border
		inGame = snake.checkBoardCollisions(newHeadPoint, this);

		if (!inGame) {
			timer.stop();
		}

		// Check if the apple should be eaten
		snake.checkApple(newHeadPoint, apple);

		// Move snake body
		snake.moveSnakeBody(newHeadPoint);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		moveSnake(snake.calculateNextHeadPoint());
		repaint();
	}

}
