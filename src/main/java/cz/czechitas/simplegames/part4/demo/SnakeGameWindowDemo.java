package cz.czechitas.simplegames.part4.demo;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;

public class SnakeGameWindowDemo extends JFrame {

	private static final long serialVersionUID = 3626538118362485936L;

	private static final String MENU_PANEL = "Menu";
	private static final String GAME_PANEL = "Game";
	
	private JPanel mainPanel;
	private JPanel menuPanel;
	private SnakeGameBoardDemo gamePanel;
	
	private CardLayout cardLayout;

	private JTextField userNameInputField;
//	private JLabel lblShowUserName;
	private JLabel lblEnterUserName;
	private JButton startGameButton;

	/**
	 * Create the application.
	 */
	public SnakeGameWindowDemo() {
		initialize();
		addComponents();		
		pack();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setTitle("Snake Game");
//		setPreferredSize(new Dimension(500, 300));
		setResizable(false);
		setLocationRelativeTo(null);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void addComponents() {
		mainPanel = (JPanel) getContentPane();
		mainPanel.setBackground(Color.white);
		
		cardLayout = new CardLayout(5, 5);
		mainPanel.setLayout(cardLayout);
		
		menuPanel = new JPanel();
		menuPanel.setLayout(null);
		menuPanel.setBackground(Color.gray);
		mainPanel.add(MENU_PANEL, menuPanel);
		
		gamePanel = new SnakeGameBoardDemo();
		gamePanel.setLayout(null);
		gamePanel.setBackground(Color.black);
		gamePanel.setFocusable(true);
		mainPanel.add(GAME_PANEL, gamePanel);
		
		startGameButton = new JButton("Start Game");
		startGameButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		startGameButton.setBounds(210, 123, 110, 30);
		menuPanel.add(startGameButton);
		
		lblEnterUserName = new JLabel("Enter User Name:");
		lblEnterUserName.setForeground(Color.WHITE);
		lblEnterUserName.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblEnterUserName.setBounds(82, 81, 110, 30);
		menuPanel.add(lblEnterUserName);
		
		userNameInputField = new JTextField();
		userNameInputField.setBounds(200, 82, 150, 30);
		userNameInputField.setColumns(20);
		menuPanel.add(userNameInputField);
		
//		lblShowUserName = new JLabel();
//		lblShowUserName.setText("No User Name Provided!");
//		lblShowUserName.setForeground(Color.WHITE);
//		lblShowUserName.setFont(new Font("Tahoma", Font.BOLD, 14));
//		lblShowUserName.setBounds(82, 81, 300, 30);
//		gamePanel.add(lblShowUserName);
		
		startGameButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showGamePanel();
			}
		});
		
		userNameInputField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					showGamePanel();
				} 
			}
		});
		
		gamePanel.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				cardLayout.show(mainPanel, MENU_PANEL);
			}
		});
		
		gamePanel.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				int key = e.getKeyCode();
				
				if (key == KeyEvent.VK_SPACE) {
					cardLayout.show(mainPanel, MENU_PANEL);
					userNameInputField.grabFocus();
				} else if (key == KeyEvent.VK_UP) {
					gamePanel.changeDirection(GameUtilitiesDemo.UP);
				} else if (key == KeyEvent.VK_LEFT) {
					gamePanel.changeDirection(GameUtilitiesDemo.LEFT);
				} else if (key == KeyEvent.VK_DOWN) {
					gamePanel.changeDirection(GameUtilitiesDemo.DOWN);
				} else if (key == KeyEvent.VK_RIGHT) {
					gamePanel.changeDirection(GameUtilitiesDemo.RIGHT);
				} 
			}
		});
	}
	
	private void showGamePanel() {
		if (!userNameInputField.getText().isEmpty()) {
//			lblShowUserName.setText("Your name is " + userNameInputField.getText() + "!");
			gamePanel.setUserName(userNameInputField.getText());
			gamePanel.initGameObjects();
			cardLayout.show(mainPanel, GAME_PANEL);
			gamePanel.grabFocus();
		} else {
			JOptionPane.showMessageDialog(SnakeGameWindowDemo.this, "Please provide the user name!");
		}
	}


}
