package cz.czechitas.simplegames.part4.demo;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;

public class AppleDemo {
	
	private PointDemo appleLocation;
	private Image appleImg;
	
	private int boardWidth;
	private int boardHeight;
	
	public AppleDemo(int boardWidth, int boardHeight) {
		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
		
		appleImg = GameUtilitiesDemo.loadImage("apple.png");
		setRandomLocation();
	}
	
	public void setRandomLocation() {
		// Calculate random X
		int x = GameUtilitiesDemo.getRandomInt(boardWidth);
		// Calculate random Y
		int y = GameUtilitiesDemo.getRandomInt(boardHeight);
		
		// Set new coordinates to the Point
		appleLocation = new PointDemo(x, y);
	}
	
	public void drawApple(Graphics g, ImageObserver observer) {
		g.drawImage(appleImg, appleLocation.getX(), appleLocation.getY(), observer);		
	}
	
	public int getX() {
		return appleLocation.getX();
	}
	
	public int getY() {
		return appleLocation.getY();
	}

}
