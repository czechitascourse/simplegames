package cz.czechitas.simplegames.part4.classroom;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;
import java.util.ArrayList;

import javax.swing.JPanel;

public class Snake {
	private Image headImg;
	private Image bodyImg;
	
	private ArrayList<Point> listOfPoints = new ArrayList<>();
	
	public Snake() {
		headImg = GameUtilities.loadImage("head.png");
		bodyImg = GameUtilities.loadImage("body.png");

		for (int i = 0; i < GameUtilities.INIT_DOTS; i++) {
			Point newPoint = new Point(50 - i * GameUtilities.STEP, 50);
			listOfPoints.add(newPoint);
		}
	}
	
	public void drawSnake(Graphics g, ImageObserver observer) {
		for (int i = listOfPoints.size() - 1; i > 0; i--) {
			Point p = listOfPoints.get(i);

			g.drawImage(bodyImg, p.getX(), p.getY(), observer);
		}
		g.drawImage(headImg, listOfPoints.get(0).getX(), listOfPoints.get(0).getY(), observer);
	}
	
	public void moveSnakeBody(Point newHeadPoint) {

		// Start with the last segment and loop through all but the head
		for (int i = listOfPoints.size() - 1; i > 0; i--) {

			Point currentPoint = listOfPoints.get(i); // segment No. i
			Point previousPoint = listOfPoints.get(i - 1); // segment No. i-1

			// Set the current segment X coordinate to the previous segment's X
			int prevX = previousPoint.getX();
			currentPoint.setX(prevX);

			// Set the current segment Y coordinate to the previous segment's Y
			int prevY = previousPoint.getY();
			currentPoint.setY(prevY);

			// Short version:
			// listOfPoints.get(i).setX(listOfPoints.get(i-1).getX());
			// listOfPoints.get(i).setY(listOfPoints.get(i-1).getY());
		}
		
		listOfPoints.set(0, newHeadPoint);
	}
	
	public void checkApple(Point snakeHeadPoint, Apple apple) {
		// Check the apple
		if (snakeHeadPoint.getX() == apple.getX() 
				&& snakeHeadPoint.getY() == apple.getY()) {
			listOfPoints.add(new Point());

			apple.setRandomLocation();
		}
	}
	
	public boolean checkBoardCollisions(Point snakeHeadPoint, JPanel gameBoard) {
		// Check the borders
		int x = snakeHeadPoint.getX();
		int y = snakeHeadPoint.getY();
		
		if (x < 0 || x >= gameBoard.getWidth() || y < 0 || y >= gameBoard.getHeight()) {
			return false;
		}
		
		// Check the body
		for (Point point : listOfPoints) {
			if (point.getX() == x && point.getY() == y) {
				return false;
			}
		} 
		return true;
	}
	
	public Point getHeadPoint() {
		return listOfPoints.get(0);
	}
	
}
