package cz.czechitas.simplegames.part4.classroom;

import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class SnakeGameWindow {
	
	private JFrame frame;

	private static final String MENU_PANEL = "Menu";
	private static final String GAME_PANEL = "Game";
	
	private JPanel mainPanel;
	private JPanel menuPanel;
	private JPanel gamePanel;
	
	private CardLayout cardLayout;
	
	private JTextField userNameInputField;
	private JLabel lblShowUserName;
	private JLabel lblEnterUserName;
	private JButton startGameButton;

	/**
	 * Create the application.
	 */
	public SnakeGameWindow() {
		initialize();
		addComponents();
		
		frame.pack();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setTitle("Snake Game");
		frame.setPreferredSize(new Dimension(500, 300));
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);

		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	private void addComponents() {
		mainPanel = (JPanel) frame.getContentPane();
		mainPanel.setBackground(Color.white);
		
		cardLayout = new CardLayout(5, 5);
		mainPanel.setLayout(cardLayout);
		
		menuPanel = new JPanel();
		menuPanel.setLayout(null);
		menuPanel.setBackground(Color.gray);
		mainPanel.add(MENU_PANEL, menuPanel);
		
		gamePanel = new JPanel();
		gamePanel.setLayout(null);
		gamePanel.setBackground(Color.black);
		mainPanel.add(GAME_PANEL, gamePanel);
		
		startGameButton = new JButton("Start Game");
		
		startGameButton.setFont(new Font("Tahoma", Font.BOLD, 12));
		startGameButton.setBounds(210, 123, 110, 30);
		menuPanel.add(startGameButton);
		
		lblEnterUserName = new JLabel("Enter User Name:");
		lblEnterUserName.setForeground(Color.WHITE);
		lblEnterUserName.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblEnterUserName.setBounds(82, 81, 110, 30);
		menuPanel.add(lblEnterUserName);
		
		userNameInputField = new JTextField();
		userNameInputField.setToolTipText("Enter User Name Here...");
		userNameInputField.setBounds(200, 82, 150, 30);
		userNameInputField.setColumns(20);
		menuPanel.add(userNameInputField);
		
		lblShowUserName = new JLabel();
		lblShowUserName.setText("No User Name Provided!");
		lblShowUserName.setForeground(Color.WHITE);
		lblShowUserName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblShowUserName.setBounds(82, 81, 300, 30);
		gamePanel.add(lblShowUserName);
		
		startGameButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				showUserName();
			}

		});
		
		userNameInputField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) {
					showUserName();
				}
			}
		});
		
		gamePanel.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_SPACE) {
					cardLayout.show(mainPanel, MENU_PANEL);
					userNameInputField.grabFocus();
				}
			}
		});
	}
	

	private void showUserName() {
		if (!userNameInputField.getText().isEmpty()) {
			lblShowUserName.setText("Your name is " + userNameInputField.getText() + "!");
			cardLayout.show(mainPanel, GAME_PANEL);
			gamePanel.grabFocus();
		} else {
			JOptionPane.showMessageDialog(frame, "Please provide the user name!");
		}
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SnakeGameWindow snakeGameWindow = new SnakeGameWindow();
					snakeGameWindow.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
