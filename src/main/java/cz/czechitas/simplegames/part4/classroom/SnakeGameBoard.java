package cz.czechitas.simplegames.part4.classroom;

import java.awt.Graphics;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import cz.czechitas.simplegames.game.wrapper.CustomGameWindow;
import cz.czechitas.simplegames.game.wrapper.IGameProcessor;

public class SnakeGameBoard implements IGameProcessor {

	CustomGameWindow window = new CustomGameWindow("Snake Game");
	JPanel gameBoard = window.getBoard();

	private Apple apple;
	private Snake snake;

	private boolean inGame;

	public SnakeGameBoard() {

		// Need to add the game processor to the game window
		window.addProcessor(this);

		apple = new Apple(window.getBoardWidth(), window.getBoardHeight());
		snake = new Snake();

		inGame = true;
	}

	@Override
	public void doDrawing(Graphics g) {
		
		drawGame(g);
		if (!inGame) {
			GameUtilities.drawGameOver(g, gameBoard);
		}
	}

	private void drawGame(Graphics g) {

		// Call draw snake
		snake.drawSnake(g, gameBoard);
		// Call draw apple
		apple.drawApple(g, gameBoard);
	}

	@Override
	public void upKeyPressed() {
		// Get head point
		Point headPoint = snake.getHeadPoint();
		
		int newHeadYCoord = headPoint.getY() - GameUtilities.STEP;
		Point newHeadPoint = new Point(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	@Override
	public void leftKeyPressed() {

		Point headPoint = snake.getHeadPoint();
		int newHeadXCoord = headPoint.getX() - GameUtilities.STEP;
		Point newHeadPoint = new Point(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	@Override
	public void downKeyPressed() {

		Point headPoint = snake.getHeadPoint();
		int newHeadYCoord = headPoint.getY() + GameUtilities.STEP;
		Point newHeadPoint = new Point(headPoint.getX(), newHeadYCoord);

		moveSnake(newHeadPoint);

		window.repaintBoard();
	}

	@Override
	public void rightKeyPressed() {

		Point headPoint = snake.getHeadPoint();
		int newHeadXCoord = headPoint.getX() + GameUtilities.STEP;
		Point newHeadPoint = new Point(newHeadXCoord, headPoint.getY());

		moveSnake(newHeadPoint);
		
		window.repaintBoard();
	}

	private void moveSnake(Point newHeadPoint) {
		// Check if the next move will cross the screen border
		inGame = snake.checkBoardCollisions(newHeadPoint, gameBoard);

		// Check if the apple should be eaten
		snake.checkApple(newHeadPoint, apple);
		
		// Move snake body
		snake.moveSnakeBody(newHeadPoint);
		
	}

	@Override
	public JFrame start() {
		return window.getWindow();
	}

}
