package cz.czechitas.simplegames.part4.classroom;

import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.ImageObserver;

public class Apple {
	
	private Point appleLocation;
	private Image appleImg;
	
	private int boardWidth;
	private int boardHeight;
	
	public Apple(int boardWidth, int boardHeight) {
		this.boardWidth = boardWidth;
		this.boardHeight = boardHeight;
		
		appleImg = GameUtilities.loadImage("apple.png");
		setRandomLocation();
	}
	
	public void setRandomLocation() {
		// Calculate random X
		int x = GameUtilities.getRandomInt(boardWidth);
		// Calculate random Y
		int y = GameUtilities.getRandomInt(boardHeight);
		
		// Set new coordinates to the Point
		appleLocation = new Point(x, y);
	}
	
	public void drawApple(Graphics g, ImageObserver observer) {
		g.drawImage(appleImg, appleLocation.getX(), appleLocation.getY(), observer);		
	}
	
	public int getX() {
		return appleLocation.getX();
	}
	
	public int getY() {
		return appleLocation.getY();
	}

}
